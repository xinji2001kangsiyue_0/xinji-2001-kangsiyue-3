import requests
import allure

@allure.feature('宠物商店宠物信息接口测试')
class TestPetStoreAPI:
    @allure.title('冒烟测试1')
    def test_add_pet(self):
        with allure.step('准备新增宠物请求数据'):
            headers = {'Content-Type':'application/json'}
            data = {"ID":1,"NAME":"tiger","status":"not for sale"}
        with allure.step('发送新增宠物请求'):
            r = requests.post('https://petstore.swagger.io/v2/pet',
                headers = headers, json = data)
        with allure.step('检查新增宠物请求响应状态码'):
            assert r.status_code == 200

    @allure.title('冒烟测试2')
    def test_find_pet_by_status(self):
        with allure.step('准备宠物请求数据'):
            p = {'status':'available'}
        with allure.step('发送查询宠物请求'):
            r = requests.get('https://petstore.swagger.io/v2/pet/findByStatus',params=p)
        with allure.step('检查查询宠物请求响应状态码'):
            assert r.status_code == 200

